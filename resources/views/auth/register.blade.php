@extends('layouts.app')

@section('body-class','signup-page sidebar-collapse')

@section('content')
<div class="page-header header-filter" filter-color="purple" style="background-image: url('{{ asset('img/biblioteca.jpg') }}'">
        <div class="container">
            <div class="row">
                <div class="col-md-10 ml-auto mr-auto">
                    <div class="card card-signup">
                        <h2 class="card-title text-center">Registro nuevo usuario</h2>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5 ml-auto">
                                    <img class="rounded-circle img-fluid" src="{{ asset('img/login.png') }}">
                                </div>

                                <div class="col-md-5 mr-auto">
                                    <div class="social text-center">

                                        <button class="btn btn-just-icon btn-round btn-google-plus">
                                            <i class="fa fa-google-plus"></i>
                                        </button>
                                        <button class="btn btn-just-icon btn-round btn-facebook">
                                            <i class="fa fa-facebook"> </i>
                                        </button>
                                        <h4> Completa tus datos </h4>
                                    </div>


                                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                    {{ csrf_field() }}
                                        <!-- Campo nombre -->
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="material-icons">face</i>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Nombre" name="name" value="{{ old('name') }}" required autofocus>
                                            </div>
                                        </div>
                                        <!-- campo email -->
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="material-icons">mail</i>
                                                </span>
                                                </div>
                                                <input id="email" type="email" placeholder="Email..." class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                            </div>
                                        </div>
                                        <!-- campo contraseña -->
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="material-icons">lock_outline</i>
                                                </span>
                                                </div>
                                                <input placeholder="Contraseña" id="password" type="password" class="form-control" name="password" required />
                                            </div>
                                        </div>
                                        <!-- Confirmar contraseña -->
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="material-icons">lock_outline</i>
                                                </span>
                                                </div>
                                                <input placeholder="Confirmar contraseña" type="password" class="form-control" name="password_confirmation" required />
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary btn-round">Confirmar registro</>
                                        </div>
                                    <!--a class="btn btn-link" href="{{ route('password.request') }}">
                                        Forgot Your Password?
                                    </a-->
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @include('includes.footer')

</div>
@endsection
