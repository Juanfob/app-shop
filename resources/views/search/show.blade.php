@extends('layouts.app')

@section('title', 'UAZON Resultados búsqueda')

@section('body-class', 'profile-page sidebar-collapse')

@section('content')

<div class="page-header header-filter" data-parallax="true" style="background-image: url('{{ asset('img/biblioteca.jpg') }}')"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 ml-auto mr-auto">
                    <div class="profile">
                        <div class="avatar">
                            <img src="{{ asset('/img/search.jpg') }}"
                                 alt="Imagen de una lupa" class="img-circle img-fluid">
                        </div>
                        <div class="name">
                            <h3 class="title">Resultados de búsqueda</h3>
                        </div>
                        <div class="description text-center">
                            <p class="title">Se encontraron {{ $products->count() }} libros para el término {{ $query }}.</p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="team">
                <div class="row text-center">
                    @foreach($products as $product)
                        <div class="card col-md-4">
                            <div class="team-player">
                                <div class="card card-plain">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <img src="{{ $product->featured_image_url }}" alt="Imagen de producto" class="img-fluid">
                                    </div>
                                    <h4 class="card-title">
                                        <a href="{{ url('/products/'.$product->id) }}">{{ $product->name }}</a>
                                        <br>
                                        <small class="card-description text-muted">{{ $product->category_name }}</small>
                                        <p>{{ $product->description }}</p>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="text-center">
                    {{ $products->links() }}
                </div>
            </div>

        </div>
    </div>
</div>


@include('includes.footer')
@endsection





