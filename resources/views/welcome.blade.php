@extends('layouts.app')

@section('title', 'Bienvenido a UAZON'))

@section('body-class', 'profile-page sidebar-collapse')

@section('styles')
    <!-- Generamos un estilo para arreglar que se vea bien los libros. -->
    <!-- Generamos que todas las columnas tengan la mismas medida -->
    <style>
        .team .row .col-md-4{
            margin-bottom: 5em;
        }

        .team .row{
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            flex-wrap: wrap;
        }
        .team .row > [class*='col-'] {
            display: flex;
            flex-direction: column;
        }

        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {    /* used to be tt-dropdown-menu in older versions */
            width: 220px;
            margin-top: 4px;
            padding: 4px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            line-height: 24px;
        }

        .tt-suggestion.tt-cursor,.tt-suggestion:hover {
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }


    </style>
@endsection

@section('content')
<div class="page-header header-filter" data-parallax="true" style="background-image: url('{{ asset('img/biblioteca.jpg') }}">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="title">Bienvenidos a UAZON</h1>
                <h4>Realiza pedidos en línea y te contactaremos para condicionar la entrega.</h4>
                <br>
            </div>
        </div>
    </div>
</div>

<div class="main main-raised">
    <div class="container">
        <div class="section text-center">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <h2 class="title">¿Por qué UAZON?</h2>
                </div>
            </div>
            <!-- Carousel  - me falta enlazar las imágenes con una base de datos de ofertas o promociones-->
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img  src="https://imagessl.casadellibro.com/t1e/c/63/10.05.18_CUADERNOVACACIONES_770x150.jpg" alt="First slide" width="960" height="300">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Extensa lista de libros</h5>
                            <p> de todos las categorías y autores</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img  src="https://imagessl.casadellibro.com/t1e/c/63/18.05.18_FERIALIBRO_370x300.jpg" alt="Second slide" width="960" height="300">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Compare precios</h5>
                            <p>somos los más económicos</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="section text-center">
            <!-- Buscador -->
            <h2 class="title">Categorías de libros</h2>
            <form method="get" action="{{ url('/search') }}">
                <input type="text" placeholder="¿que libro buscas?" class="form-control" name="query" id="search">
                <button class="btn btn-primary btn-just-icon" type="submit">
                    <i class="material-icons">search</i>
                </button>
            </form>
            <div class="team">
                <div class="row">
                    @foreach($categories as $category)
                    <div class="card col-md-4">
                        <div class="team-player">
                            <div class="card card-plain">
                                <div class="col-md-6 ml-auto mr-auto">
                                    <img src="{{ $category->featured_image_url }}" alt="Imagen representativa de la categoria" class="img-fluid">
                                </div>
                                <h4 class="card-title">
                                    <a href="{{ url('/categories/'.$category->id) }}">{{ $category->name }}</a>
                                </h4>
                                <p class="description">{{ $category->description }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="text-center">
                </div>
            </div>
        </div>
        <div class="section section-contacts">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <h2 class="text-center title">¿Aún no te has registrado?</h2>
                    <h4 class="text-center description">Regístrate ingresando tus datos básicos, y podrás realizar tus pedidos a través de nuestro carrito de compras. Si aún no te decides
                    , de todas formas, con tu cuenta de usuario podrás hacer todas tus consultas sin compromiso.</h4>
                    <form class="contact-form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Nombre</label>
                                    <input type="email" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Correo electrónico</label>
                                    <input type="email" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleMessage" class="bmd-label-floating">Tu mensaje</label>
                            <textarea type="email" class="form-control" rows="4" id="exampleMessage"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-4 ml-auto mr-auto text-center">
                                <button class="btn btn-primary btn-raised">
                                    Enviar consulta
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('includes.footer')
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/typeahead.bundle.min.js') }}"></script>
    <script>
        $(function () {
            // motor de sugerencias
            var products = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                //  local is an array
                //local: ['hola', 'prueba1', 'prueba2', ' hola2', 'abdece']
                prefetch: {
                    url: '{{ url('/products/json') }}',
                    cache: false
                }
            });

            // Inicializamos typeahead sobre nuestro input de búsqueda
            $('#search').typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, {
                name: 'products',
                source: products
            })
        })
    </script>
@endsection
