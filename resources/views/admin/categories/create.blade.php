@extends('layouts.app')

@section('title', 'Bienvenido a App Shop')

@section('body-class', 'profile-page sidebar-collapse')

@section('content')
<div class="page-header header-filter" data-parallax="true" style="background-image: url('{{ asset('img/biblioteca.jpg') }}">

</div>

<div class="main main-raised">
    <div class="container">

        <div class="section">
            <h2 class="title text-center">Registrar nueva categoría</h2>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="post" action="{{ url('/admin/categories') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nombreCategoria" class="bmd-label-floating">Nombre de la categoría</label>
                            <input type="text" class="form-control" id="nombreCategoria" name="name" value="{{ old('name') }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="descCategoria" class="bmd-label-floating">Descripcion de la categoría</label>
                    <input type="text" class="form-control" id="descCategoria" name="description" value="{{ old('description') }}">
                </div>

                <button type="submit" class="btn btn-primary">Registrar categoría</button>
                <a href="{{ url('/admin/categories') }}" class="btn btn-default">Cancelar</a>
            </form>
        </div>

    </div>
</div>

@include('includes.footer')
@endsection
