@extends('layouts.app')

@section('title', 'Bienvenido a App Shop')

@section('body-class', 'profile-page sidebar-collapse')

@section('content')
<div class="page-header header-filter" data-parallax="true" style="background-image: url('{{ asset('img/biblioteca.jpg') }}">

</div>

<div class="main main-raised">
    <div class="container">

        <div class="section">
            <h2 class="title text-center">Editar producto seleccionado</h2>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="post" action="{{ url('/admin/products/'.$product->id.'/edit') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nombreProducto" class="bmd-label-floating">Nombre del producto</label>
                            <input type="text" class="form-control" id="nombreProducto" name="name" value="{{ old('name', $product->name) }}">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="precioProducto" class="bmd-label-floating">Precio del producto</label>
                            <input type="number" step="0.01" class="form-control" id="precioProducto" name="price" value="{{ old('price',$product->price) }}">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="descProducto" class="bmd-label-floating">Descripcion corta del producto</label>
                            <input type="text" class="form-control" id="descProducto" name="description" value="{{ old('description') }}">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Categoría</label>
                            <select class="form-control" name="category_id">
                                <option value="0">General</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" @if($category->id ===
                                    old('$category_id',$product->category_id)) selected @endif>
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="descLargaProducto">Descripción extensa del producto</label>
                    <textarea class="form-control" id="descLargaProducto" rows="5" name="long_description">{{ old('long_descrition', $product->long_description) }}</textarea>
                </div>

                <button type="submit" class="btn btn-primary">Guardar cambios</button>
                        <a href="{{ url('/admin/products') }}" class="btn btn-default">Cancelar</a>
            </form>
        </div>

    </div>
</div>

@include('includes.footer')
@endsection
