@extends('layouts.app')

@section('title', 'Listado de productos')

@section('body-class', 'profile-page sidebar-collapse')

@section('content')
<div class="page-header header-filter" data-parallax="true" style="background-image: url('{{ asset('img/biblioteca.jpg') }}">

</div>

<div class="main main-raised">
    <div class="container">
        <div class="section text-center">
            <h2 class="title">Listado de Productos</h2>
            <div class="team">
                <div class="row">
                    <a href="{{ url('/admin/products/create') }}" class="btn btn-primary btn-round">Nuevo producto</a>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Nombre</th>
                            <th class="col-md-4 text-center">Descripcion</th>
                            <th class="text-center">Categoría</th>
                            <th class="text-right">Precio</th>
                            <th class="text-right">Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                            <tr>
                                <td class="text-center">{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->description }}</td>
                                <!-- Si el producto tiene una categoría se muestra, y si no 'General' -->
                                <td>{{ $product->category_name}}</td>
                                <td class="text-right">{{ $product->price }} &euro;</td>
                                <td class="td-actions text-right">
                                   <form method="post" action="{{ url('/admin/products/'.$product->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                       <a href="{{ url('/products/'.$product->id) }}" rel="tooltip" title="Ver producto" class="btn btn-info btn-link btn-xs" target="_blank">
                                           <i class="fa fa-info"></i>
                                       </a>
                                       <a href="{{ url('/admin/products/'.$product->id.'/edit') }}" rel="tooltip" title="Editar producto" class="btn btn-success btn-link btn-xs">
                                           <i class="fa fa-edit"></i>
                                       </a>
                                       <a href="{{ url('/admin/products/'.$product->id.'/images') }}" rel="tooltip" title="Imágenes del producto" class="btn btn-warning btn-link btn-xs">
                                           <i class="fa fa-image"></i>
                                       </a>
                                        <button type="submit" rel="tooltip" title="Borrar producto" class="btn btn-danger btn-link btn-xs">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                        {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@include('includes.footer')
@endsection
