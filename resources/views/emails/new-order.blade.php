<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nuevo pedido</title>
</head>
<body>
    <p>Se ha realizado un nuevo pedido.</p>
    <p>Estos son los datos del cliente que realizó el pedido:</p>
    <ul>
        <li>
            <strong>Nombre:</strong>
            {{ $user->name }}
        </li>
        <li>
            <strong>E-mail:</strong>
            {{ $user->email }}
        </li>
        <li>
            <strong>Fecha del pedido:</strong>
            {{ $cart->order_date }}
        </li>
    </ul>
    <p>
        <a href="{{ url('/admin/order/'.$cart->id) }}">Haz click aqui</a>
        para ver más información sobre este pedido.
    </p>
</body>
</html>