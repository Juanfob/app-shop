<?php

Route::get('/', 'TestController@welcome');

Auth::routes();

Route::get('/search', 'SearchController@show');
Route::get('/products/json', 'SearchController@data');

Route::get('/home','HomeController@index')->name('home');
Route::get('/products/{id}', 'ProductController@show'); // muestra la ficha de un producto
Route::get('/categories/{category}', 'CategoryController@show');

Route::post('/cart', 'CartDetailController@store'); // Alamacena los detalles del carrito asociado al cliente
Route::delete('/cart', 'CartDetailController@destroy'); // Elimina una línea del carrito activo de compra
Route::post('/order', 'CartController@update'); // Actualiza el carrito activo iniciado, con las lineas de pedido


// Aplicamos al grupo de rutas el middleware de admin, y si se pasa este se ejecuta el de admin
Route::middleware(['auth','admin'])->group(function () {
    Route::get('/admin/products', 'Admin\ProductController@index'); // listado de productos
    Route::get('/admin/products/create', 'Admin\ProductController@create');// Formulario para nuevos productos
    Route::post('/admin/products', 'Admin\ProductController@store'); // Almacena el nuevo producto en la BD
    Route::get('/admin/products/{id}/edit', 'Admin\ProductController@edit');// Formulario para nuevos productos
    Route::post('/admin/products/{id}/edit', 'Admin\ProductController@update'); // Aactualiza el nuevo producto en la BD
    Route::delete('/admin/products/{id}', 'Admin\ProductController@destroy');// Elimina el producto en la BD

    Route::get('/admin/products/{id}/images', 'Admin\ImageController@index'); // muestra las imágenes de un producto
    Route::post('/admin/products/{id}/images', 'Admin\ImageController@store');
    Route::delete('/admin/products/{id}/images', 'Admin\ImageController@destroy');
    Route::get('/admin/products/{id}/images/select/{image}', 'Admin\ImageController@select'); // Destacar imagen

    Route::get('/admin/categories', 'Admin\CategoryController@index'); // listado
    Route::get('/admin/categories/create', 'Admin\CategoryController@create');// Formulario para crear nuevos
    Route::post('/admin/categories', 'Admin\CategoryController@store'); // Almacena en la BD
    Route::get('/admin/categories/{category}/edit', 'Admin\CategoryController@edit');// Formulario para edicion
    Route::post('/admin/categories/{category}/edit', 'Admin\CategoryController@update'); // Actualizar
    Route::delete('/admin/categories/{category}', 'Admin\CategoryController@destroy');// Eliminar

});




