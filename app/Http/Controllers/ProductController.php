<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductImage;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function show ($id){
        $product = Product::find($id);
        //Buscamos las imagenes asociadas al libro
        $images = $product->images;
        //Creamos las imagenes de la derecha y las de la izquierda
        $imagesLeft = collect();
        $imagesRight = collect();
        //REcorremos el array de imagenes para asignarla a cada array dependiendo de si es par o no
        foreach ($images as $key => $image){
            if ($key%2==0){
                $imagesLeft->push($image);
            }else{
                $imagesRight->push($image);
            }
        }

        return view('products.show')->with(compact('product', 'imagesLeft', 'imagesRight'));
    }
}
