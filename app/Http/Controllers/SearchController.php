<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function show(Request $request)
    {
        $query = $request->input('query');

        $products = Product::where('name', 'like', "%$query%")->paginate(5);
        // Redirigimos a la pagina del producto cuando se solo uno
        if ($products->count() === 1){
            $id = $products->first()->id;
            return redirect("/products/$id"); // == '/products/.$id
        }
        return view('search.show')->with(compact('products', 'query'));
    }

    // Función que devuelve un arreglo Json para el motor de sugerencias para buscar
    public function data()
    {
        //The pluck method retrieves all of the values for a given key:
        $products = Product::pluck('name');
        return $products;
    }
}
