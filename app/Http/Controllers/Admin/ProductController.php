<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::paginate(10);
        return view('admin.products.index')->with(compact('products')); // Listado de productos
    }

    public function create()
    {
        $categories = Category::orderBy('name')->get();
        return view('admin.products.create')->with(compact('categories')); // formulario de registro
    }

    public function store(Request $request)
    {
        // Validamos los datos
        $messages = [
            'name.required' => 'Es necesario introducir un nombre de producto.',
            'name.min' => 'El nombre del producto debe tener al menos 3 carácteres.',
            'description.required' =>'La descripción corta es un campo obligatorio',

        ];
        $rules = [
            'name' => 'required|min:3',
            'description' => 'required|max:200',
            'price' => 'required|numeric|min:0',
        ];
        $this->validate($request, $rules, $messages);

        // registrar el nuevo producto en la bd
        //dd($request->all()); // permite imprimir $request y finalizar la ejecución del programa.
        $product = new Product();
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->long_description = $request->input('long_description');
        $product->category_id = $request->input('category_id');
        $product->save(); // realiza el insert
        //  hacemos una redirección al listado de productos
        return redirect('/admin/products');
    }

    public function edit($id)
    {
        $categories = Category::orderBy('name')->get();
        $product = Product::find($id);
        return view('admin.products.edit')->with(compact('product','categories')); // formulario de registro
    }

    public function update(Request $request, $id)
    {
        // Validamos los datos
        $messages = [
            'name.required' => 'Es necesario introducir un nombre de producto.',
            'name.min' => 'El nombre del producto debe tener al menos 3 carácteres.',
            'description.required' =>'La descripción corta es un campo obligatorio',

        ];
        $rules = [
            'name' => 'required|min:3',
            'description' => 'required|max:200',
            'price' => 'required|numeric|min:0',
        ];
        $this->validate($request, $rules, $messages);

        // Actualizamos el producto en la bd
        //dd($request->all()); // permite imprimir $request y finalizar la ejecución del programa.
        $product = Product::find($id);
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->long_description = $request->input('long_description');
        $product->category_id = $request->input('category_id');
        $product->save(); // realiza el Update
        //  hacemos una redirección al listado de productos
        return redirect('/admin/products');
    }

    public function destroy($id)
    {
        // elimina el producto en la bd
        $product = Product::find($id);
        $product->delete(); // realiza el Delete

        return back();
    }
}
