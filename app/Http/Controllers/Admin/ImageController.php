<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Product;
use App\ProductImage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ImageController extends Controller
{
    public function index($id)
    {
        //mostramos las imagenes asociadas a un producto
        $product = Product::find($id);
        $images = $product->images()->orderBy('featured', 'desc')->get();
        return view ('admin.products.images.index')->with(compact('product','images'));
    }

    public function store(Request $request, $id)
    {
        // vamos a guardar la imagen del producto
        $file = $request->file('photo'); // obtiene el archivo que se está subiendo
        // esta es la ruta donde vamos a guardar las imágenes
        // public_path() es la ruta absoluta a la carpeta public
        $path = public_path() . '/images/products';
        $fileName = uniqid() . $file->getClientOriginalName();
        $moved = $file->move($path, $fileName); //Creamos la variable para saber si se ha guardado correctamente.

        // Crear el registro en la tabla product_images
        if ($moved){
            $productImage = new ProductImage();
            $productImage->image = $fileName;
            //$productImage->featured = ;
            $productImage->product_id = $id;
            $productImage->save();
        }
        return back();
    }

    public function destroy(Request $request, $id)
    {
        // Eliminar el archivo
        $productImage = ProductImage::find($request->input('image_id'));
        if (substr($productImage->image,0,4) === "http"){
            $deleted = true;
        } else {
            $fullPath = public_path() . '/images/products/'. $productImage->image;
            $deleted =File::delete($fullPath);
        }

        // eliminar
        if ($deleted){
            $productImage->delete();
        }

        return back();
    }

    public function select($id, $image){
        // Vamos a poner las imágenes destacadas anteriores como no destacadas
        ProductImage::where('product_id', $id)->update([
           'featured' => false
        ]);

        // Buscamos la imagen en productImage  que hemos pasado  y la ponemos como destacada
        $productImage = ProductImage::find($image);
        $productImage->featured = true;
        $productImage->save();

        return back();
    }
}
