<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('id')->paginate(10);
        return view('admin.categories.index')->with(compact('categories')); // Listado de productos
    }

    public function create()
    {
        return view('admin.categories.create'); // formulario de registro
    }

    public function store(Request $request)
    {
        // Validamos los datos
        $messages = [
            'name.required' => 'Es necesario introducir un nombre de categoría.',
            'name.min' => 'El nombre de la categoría debe tener al menos 3 carácteres.',
            'description.required' =>'La descripción es un campo obligatorio',
            'description.max' => 'La descripción no puede superar los 200 caracteres'
        ];
        $rules = [
            'name' => 'required|min:3',
            'description' => 'required|max:200',
        ];
        $this->validate($request, $rules, $messages);

        // registrar la nueva categoria en la bd
        // mediante asignacion masiva del request
        Category::create($request->all()); // Esto nos trae todos los campos que el formulario envia
        /* para que esto funcione hay que definir la variable protected en el modelo
        Category e indicar que campos son posibles de llenar con una carga masiva de datos*/
        //  hacemos una redirección al listado de productos
        return redirect('/admin/categories');
    }

    public function edit(Category $category) //hacemos la busqueda de forma implicita y el find lo hace laravel
    {
        return view('admin.categories.edit')->with(compact('category')); // formulario de registro
    }

    public function update(Request $request, Category $category)
    {
        //  Esta es otra manera de validar los datos, poniento como public stacic $rules y $messages en el modelo
        $this->validate($request, Category::$rules, Category::$messages);

        // Actualizamos la categoría en la bd
        $category->update($request->all());
        //  hacemos una redirección al listado de categorias
        return redirect('/admin/categories');
    }

    public function destroy(Category $category)
    {

        $category->delete(); // realiza el Delete

        return back();
    }
}
