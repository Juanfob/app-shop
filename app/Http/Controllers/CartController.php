<?php

namespace App\Http\Controllers;

use App\Mail\NewOrder;
use App\User;
use Carbon\Carbon; // esta clase es genial para trabajar con fechas
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CartController extends Controller
{
    // Este método deja el carrito en pendiente para que un administrador pueda aceptarlo o cancelarlo
    public function update()
    {
        $client =  auth()->user();
        $cart = $client->cart;
        $cart->status = 'Pending';
        $cart->order_date = Carbon::now();
        $cart->save();

        $admins = User::where('admin', true)->get(); // para sacar todos los administradores y enviar el emal
        Mail::to($admins)->send(new NewOrder($client, $cart));

        $notification = 'Tu pedido se ha registrado correctamente. Te contactaremos pronto via mail.';
        return back()->with(compact('notification'));
    }
}
