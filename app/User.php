<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Función que nos devuelve la relación de los carts que tiene un usuario.
    public function carts(){
        return $this->hasMany(Cart::class);
    }

    // Nos devuelve los carritos activos que tiene el usuario
    public function getCartAttribute(){
        // Si hay carrito de compra activo devolvemos el id
        $cart = $this->carts()->where('status','Active')->first();
        if ($cart)
            return $cart;

        // Si no, creamos uno nuevo
        $cart = new Cart();
        $cart->status = 'Active';
        $cart->user_id = $this->id;
        $cart->save();
        // guardamos y devolvemos el carrito id creado
        return $cart;
    }
}
