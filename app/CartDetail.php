<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartDetail extends Model
{
    public function product(){
        //Un detalle  de un carrito de compra pertenece a un producto
        //CattDetail N           1 Product
        return $this->belongsTo(Product::class);
    }
}
