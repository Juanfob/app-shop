<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // Validamos los datos
    public static $messages = [
        'name.required' => 'Es necesario introducir un nombre de categoría.',
        'name.min' => 'El nombre de la categoría debe tener al menos 3 carácteres.',
        'description.required' =>'La descripción es un campo obligatorio',
        'description.max' => 'La descripción no puede superar los 200 caracteres'
    ];

    public static $rules = [
        'name' => 'required|min:3',
        'description' => 'required|max:200'
    ];

    // Este arreglo es el que se va a llenar masivamente
    protected $fillable= ['name','description'];
    // $category->products
    public function products(){
        return $this->hasMany(Product::class);
    }

    public function getFeaturedImageUrlAttribute()
    {
        $featuredProduct = $this->products()->first();
        return $featuredProduct->featured_image_url;
    }
}
