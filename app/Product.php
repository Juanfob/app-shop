<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // $product->category
    public function category(){
        return $this->belongsTo(Category::class);
    }

    // $product-images
    public function images(){
        return $this->hasMany(ProductImage::class);
    }

    // Accessor para imagen destacada
    public function getFeaturedImageUrlAttribute(){
        // obtenemos la imagen destacada del producto
        $featuredImage= $this->images()->where('featured', true)->first();
        // en caso que un producto no tenga imagen destacada, esta será la primera imagen asociada
        if (!$featuredImage)
            $featuredImage = $this->images()->first();
        // Si encontramos una imagen destacada, devolvemos su url
        if ($featuredImage){
            return $featuredImage->url;
        }
        // Devolvemos una imagen por defecto
        return '/images/products/default.gif';
    }

    public function getCategoryNameAttribute(){
        if($this->category)
            return $this->category->name;

        return 'General';
    }
}
